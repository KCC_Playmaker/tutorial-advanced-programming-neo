package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CVController {

    @GetMapping("/cv")
    public String cv(@RequestParam(name = "name", required = true)
                             String name, Model model) {
        if (name.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", name + ", I hope you're interested to hire me");
        }
        return "cv";
    }

}
