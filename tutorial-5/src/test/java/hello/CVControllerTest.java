package hello;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CVController.class)
public class CVControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void cvPageWithName() throws Exception {
        mockMvc.perform(get("/cv").param("name", "Greg"))
                .andExpect(content().string(containsString("Greg, I hope you&#39;re interested to hire me")));
    }

    @Test
    public void cvPageWithNameNegative() throws Exception {
        mockMvc.perform(get("/cv").param("name", "Greg"))
                .andExpect(content().string(Matchers.not(containsString("This is my CV"))));
    }

    @Test
    public void cvPageWithoutName() throws Exception {
        mockMvc.perform(get("/cv").param("name", ""))
                .andExpect(content().string(containsString("This is my CV")));
    }

    @Test
    public void cvPageWithoutNameNegative() throws Exception {
        mockMvc.perform(get("/cv").param("name", ""))
                .andExpect(content().string(Matchers.not(containsString("Greg, I hope you&#39;re interested to hire me"))));
    }

}
