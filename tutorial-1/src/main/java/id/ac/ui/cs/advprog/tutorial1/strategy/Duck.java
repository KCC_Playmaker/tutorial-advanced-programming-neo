package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    // TODO Complete me!
    public void setFlyBehavior(FlyBehavior newBehavior) {
        this.flyBehavior = newBehavior;
    }

    public void setQuackBehavior(QuackBehavior newBehavior) {
        this.quackBehavior = newBehavior;
    }

    public void swim() {
        System.out.println("I am swimming.");
    }
    
    public abstract void display();
}
