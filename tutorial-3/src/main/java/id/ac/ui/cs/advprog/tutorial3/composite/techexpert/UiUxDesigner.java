package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
// import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import java.lang.IllegalArgumentException;

public class UiUxDesigner extends Employees {
    //TODO Implement
    public UiUxDesigner(String name, double salary) throws IllegalArgumentException {
        //TODO Implement
        if (salary < 90000) throw new  IllegalArgumentException();
        else {
            this.name = name;
            this.salary = salary;
            this.role = "UI/UX Designer";
        }
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}
