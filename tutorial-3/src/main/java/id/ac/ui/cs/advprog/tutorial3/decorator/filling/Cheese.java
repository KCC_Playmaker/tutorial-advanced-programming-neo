package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Filling {
    Food food;

    public Cheese(Food food) {
        //TODO Implement
        this.food = food;
        this.description = "adding cheese";
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return this.food.getDescription() + ", " + this.description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return this.food.cost() + 2.0;
    }
}
