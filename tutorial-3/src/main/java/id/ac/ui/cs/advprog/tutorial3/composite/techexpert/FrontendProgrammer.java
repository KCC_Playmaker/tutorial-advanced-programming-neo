package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
// import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import java.lang.IllegalArgumentException;

public class FrontendProgrammer extends Employees {
    //TODO Implement
    public FrontendProgrammer(String name, double salary) throws IllegalArgumentException {
        //TODO Implement
        if (salary < 30000) throw new IllegalArgumentException();
        else {
            this.name = name;
            this.salary = salary;
            this.role = "Front End Programmer";
        }
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}
