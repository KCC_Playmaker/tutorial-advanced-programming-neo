package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
// import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import java.lang.IllegalArgumentException;

public class NetworkExpert extends Employees {
    //TODO Implement
    public NetworkExpert(String name, double salary) throws IllegalArgumentException {
        //TODO Implement
        if (salary < 50000) throw new IllegalArgumentException();
        else {
            this.name = name;
            this.salary = salary;
            this.role = "Network Expert";
        }
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}
