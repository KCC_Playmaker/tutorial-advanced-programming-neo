package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        //TODO Implement
        this.employeesList.add(employees);
    }

    public double getNetSalaries() {
        //TODO Implement
        Iterator<Employees> listIterator = employeesList.iterator();
        double netSalaries = 0.0;
        while(listIterator.hasNext()) {
            Employees employee = (Employees) listIterator.next();
            netSalaries += employee.getSalary();
        }
        return netSalaries;
    }

    public List<Employees> getAllEmployees() {
        //TODO Implement
        return this.employeesList;
    }
}
