package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;

public class BreadSimulator {
    public static void main(String[] args) {
        //Create a thick burger with all ingredients
        Food burger = BreadProducer.THICK_BUN.createBreadToBeFilled();
        burger = FillingDecorator.CHEESE.addFillingToBread(burger);
        burger = FillingDecorator.BEEF_MEAT.addFillingToBread(burger);
        burger = FillingDecorator.LETTUCE.addFillingToBread(burger);
        burger = FillingDecorator.CHILI_SAUCE.addFillingToBread(burger);
        burger = FillingDecorator.CHEESE.addFillingToBread(burger);
        burger = FillingDecorator.CHICKEN_MEAT.addFillingToBread(burger);
        burger = FillingDecorator.CUCUMBER.addFillingToBread(burger);
        burger = FillingDecorator.TOMATO_SAUCE.addFillingToBread(burger);
        burger = FillingDecorator.CHEESE.addFillingToBread(burger);
        burger = FillingDecorator.BEEF_MEAT.addFillingToBread(burger);
        burger = FillingDecorator.TOMATO.addFillingToBread(burger);
        burger = FillingDecorator.CHILI_SAUCE.addFillingToBread(burger);
        burger = FillingDecorator.CHEESE.addFillingToBread(burger);

        System.out.println("A Quattro Max Cheese Burger: " + burger.getDescription());
        System.out.println("This burger costs: $" + burger.cost());
    }
}