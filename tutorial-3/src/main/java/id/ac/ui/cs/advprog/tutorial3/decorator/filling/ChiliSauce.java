package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Filling {
    Food food;

    public ChiliSauce(Food food) {
        //TODO Implement
        this.food = food;
        this.description = "adding chili sauce";
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return this.food.getDescription() + ", " + this.description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return this.food.cost() + 0.3;
    }
}
