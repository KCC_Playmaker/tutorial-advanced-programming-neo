package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.Company;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

import java.lang.IllegalArgumentException;

public class CompanySimulator {
    public static void main(String[] args) {
        //Create a company
        Company solTech = new Company();
        try {
            Employees employee1 = new Ceo("Zaizen Akira", 1000000);
            solTech.addEmployee(employee1);
            Employees employee2 = new BackendProgrammer("Fujiki Yusaku", 25000);
            solTech.addEmployee(employee2);
            Employees employee3 = new NetworkExpert("Kusanagi Shoichi", 30000);
            solTech.addEmployee(employee3);
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }
        System.out.println(solTech.getNetSalaries());
    }
}