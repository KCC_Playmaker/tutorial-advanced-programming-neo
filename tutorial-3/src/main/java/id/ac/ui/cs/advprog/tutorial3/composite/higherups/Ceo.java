package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

import java.lang.IllegalArgumentException;

public class Ceo extends Employees {
    public Ceo(String name, double salary) throws IllegalArgumentException {
        //TODO Implement
        if (salary < 200000) throw new IllegalArgumentException();
        else {
            this.name = name;
            this.salary = salary;
            this.role = "CEO";
        }
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}
