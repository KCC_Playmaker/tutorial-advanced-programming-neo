package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ExtraThinCrustDough implements Dough {
    public String toString() {
        return "Extra thin crust dough";
    }
}
