package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class JalapenoSauce implements Sauce {
    public String toString() {
        return "Jalapeno Sauce with Jalapeno slices";
    }
}
