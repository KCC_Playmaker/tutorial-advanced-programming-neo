package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class RazorClams implements Clams {

    public String toString() {
        return "Razor Clams from Pacific West Coast";
    }
}
