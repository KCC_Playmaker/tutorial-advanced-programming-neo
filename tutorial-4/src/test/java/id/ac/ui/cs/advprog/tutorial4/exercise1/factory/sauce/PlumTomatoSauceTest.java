package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PlumTomatoSauceTest {
    private Sauce plumTom;

    @Before
    public void setUp() {
        plumTom = new PlumTomatoSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Tomato sauce with plum tomatoes", plumTom.toString());
    }
}
