package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MarinaraSauceTest {
    private Sauce marina;

    @Before
    public void setUp() {
        marina = new MarinaraSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Marinara Sauce", marina.toString());
    }
}
