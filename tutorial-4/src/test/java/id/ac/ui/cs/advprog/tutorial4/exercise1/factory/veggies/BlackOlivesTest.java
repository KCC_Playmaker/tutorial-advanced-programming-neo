package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BlackOlivesTest {
    private Veggies olive;

    @Before
    public void setUp() {
        olive = new BlackOlives();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Black Olives", olive.toString());
    }
}
