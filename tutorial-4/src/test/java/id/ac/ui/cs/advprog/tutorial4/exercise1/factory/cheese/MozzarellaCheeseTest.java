package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MozzarellaCheeseTest {
    private Cheese mozza;

    @Before
    public void setUp() {
        mozza = new MozzarellaCheese();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Shredded Mozzarella", mozza.toString());
    }
}
