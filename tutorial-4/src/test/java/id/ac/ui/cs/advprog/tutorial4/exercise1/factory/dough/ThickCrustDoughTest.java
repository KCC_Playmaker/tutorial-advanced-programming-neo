package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThickCrustDoughTest {
    private Dough thickCrust;

    @Before
    public void setUp() {
        thickCrust = new ThickCrustDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("ThickCrust style extra thick crust dough", thickCrust.toString());
    }
}
