package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RedPepperTest {
    private Veggies redPepper;

    @Before
    public void setUp() {
        redPepper = new RedPepper();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Red Pepper", redPepper.toString());
    }
}
