package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaIngredientFactoryTest {
    private PizzaIngredientFactory factory;

    @Before
    public void setUp() {
        factory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testIngredientsToString() {
        assertEquals("Thin Crust Dough", factory.createDough().toString());
        assertEquals("Marinara Sauce", factory.createSauce().toString());
        assertEquals("Reggiano Cheese", factory.createCheese().toString());
        assertEquals("Fresh Clams from Long Island Sound", factory.createClam().toString());
        assertEquals("Garlic", factory.createVeggies()[0].toString());
        assertEquals("Onion", factory.createVeggies()[1].toString());
        assertEquals("Mushrooms", factory.createVeggies()[2].toString());
        assertEquals("Red Pepper", factory.createVeggies()[3].toString());

    }
}
