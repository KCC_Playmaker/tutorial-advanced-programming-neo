package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ReggianoCheeseTest {
    private Cheese reggie;

    @Before
    public void setUp() {
        reggie = new ReggianoCheese();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Reggiano Cheese", reggie.toString());
    }
}
