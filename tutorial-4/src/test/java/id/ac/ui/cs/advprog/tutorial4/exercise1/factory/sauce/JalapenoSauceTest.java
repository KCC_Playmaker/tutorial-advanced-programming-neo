package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class JalapenoSauceTest {
    private Sauce jalapeno;

    @Before
    public void setUp() {
        jalapeno = new JalapenoSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Jalapeno Sauce with Jalapeno slices", jalapeno.toString());
    }
}
