package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class EggplantTest {
    private Veggies eggy;

    @Before
    public void setUp() {
        eggy = new Eggplant();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Eggplant", eggy.toString());
    }
}
