package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {
    private PizzaIngredientFactory factory;

    @Before
    public void setUp() {
        factory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testIngredientsToString() {
        assertEquals("ThickCrust style extra thick crust dough", factory.createDough().toString());
        assertEquals("Jalapeno Sauce with Jalapeno slices", factory.createSauce().toString());
        assertEquals("Shredded Mozzarella", factory.createCheese().toString());
        assertEquals("Frozen Clams from Chesapeake Bay", factory.createClam().toString());
        assertEquals("Onion", factory.createVeggies()[0].toString());
        assertEquals("Mushrooms", factory.createVeggies()[1].toString());
        assertEquals("Red Pepper", factory.createVeggies()[2].toString());
        assertEquals("Potato", factory.createVeggies()[3].toString());
        assertEquals("Black Olives", factory.createVeggies()[4].toString());

    }
}
