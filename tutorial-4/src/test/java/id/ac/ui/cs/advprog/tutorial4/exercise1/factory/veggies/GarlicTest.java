package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GarlicTest {
    private Veggies garlic;

    @Before
    public void setUp() {
        garlic = new Garlic();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Garlic", garlic.toString());
    }
}
