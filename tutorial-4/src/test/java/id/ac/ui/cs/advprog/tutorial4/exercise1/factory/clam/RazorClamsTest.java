package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RazorClamsTest {
    private Clams raze;

    @Before
    public void setUp() {
        raze = new RazorClams();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Razor Clams from Pacific West Coast", raze.toString());
    }
}
