package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class OnionTest {
    private Veggies onion;

    @Before
    public void setUp() {
        onion = new Onion();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Onion", onion.toString());
    }
}
