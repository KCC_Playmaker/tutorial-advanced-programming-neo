package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ExtraThinCrustDoughTest {
    private Dough extraThinCrust;

    @Before
    public void setUp() {
        extraThinCrust = new ExtraThinCrustDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Extra thin crust dough", extraThinCrust.toString());
    }
}
