package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CheddarCheeseTest {
    private Cheese cheddar;

    @Before
    public void setUp() {
        cheddar = new CheddarCheese();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Sliced Cheddar", cheddar.toString());
    }
}
