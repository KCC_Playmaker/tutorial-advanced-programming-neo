package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MushroomTest {
    private Veggies mushie;

    @Before
    public void setUp() {
        mushie = new Mushroom();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Mushrooms", mushie.toString());
    }
}
