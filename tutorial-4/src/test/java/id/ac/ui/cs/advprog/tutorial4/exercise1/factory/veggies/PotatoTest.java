package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PotatoTest {
    private Veggies pota;

    @Before
    public void setUp() {
        pota = new Potato();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Potato", pota.toString());
    }
}
