package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // TODO Complete me!
        /* for (Command command : this.commands) {
            command.execute();
        } */
        this.commands.stream().forEach((command) -> command.execute());
    }

    @Override
    public void undo() {
        // TODO Complete me!
        int numOfCommands = this.commands.size();
        for (int order = numOfCommands - 1; order >= 0; order--) {
            this.commands.get(order).undo();
        }

    }
}
